import {createStore, compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './rootReducer.js';

const middlewares = applyMiddleware(thunk);

const devtools = window.devToolsExtension ? window.devToolsExtension() : f => f;

const defaultState = {
    navigation: {
        isOnHomePage: true,
        isOnHeroDetailPage: false,
        isOnComicDetailPage: false
    },
    search: {
        searchTerm: '',
        isTouched: false,
        isHeroSearch: true,
        isComicSearch: false
    },
    heroes: {
        heroList: [],
        selectedHero: null,
        selectedHeroComics: [],
        isLoading: false,
        isHeroLoading: false,
        isHeroesComicsLoading: false
    },
    comics: {
        comicList: [],
        selectedComic: null,
        selectedComicHeroes: [],
        isLoading: false,
        isComicLoading: false,
        isComicsHeroesLoading: false
    }
};

const store = createStore(rootReducer, defaultState, compose(middlewares, devtools));

export default store;