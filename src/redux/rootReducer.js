import {combineReducers} from 'redux';
import comicsReducer from '../components/comics/reducer.js';
import heroesReducer from '../components/heroes/reducer.js';
import searchReducer from '../components/search/reducer.js';
import navigationReducer from '../components/navigation/reducer.js';


const rootReducer = combineReducers({
    navigation: navigationReducer,
    comics: comicsReducer,
    heroes: heroesReducer,
    search: searchReducer
});

export default rootReducer;