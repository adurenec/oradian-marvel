
function heroesReducer(state = {}, action) {
    let newState;
    switch (action.type) {
        case 'SET_HERO_LIST':
            newState = Object.assign({}, state, {
                heroList: action.heroList
            });
            return newState;
        case 'SET_IS_HERO_LIST_LOADING':
            newState = Object.assign({}, state, {
                isLoading: action.isLoading
            });
            return newState;
        case 'SET_SELECTED_HERO':
            newState = Object.assign({}, state, {
                selectedHero: action.selectedHero
            });
            return newState;
        case 'SET_IS_HERO_DETAIL_LOADING':
            newState = Object.assign({}, state, {
                isHeroLoading: action.isLoading
            });
            return newState;
        case 'SET_SELECTED_HEROES_COMICS':
            newState = Object.assign({}, state, {
                selectedHeroComics: action.comics
            });
            return newState;
        case 'SET_IS_HERO_DETAIL_COMICS_LOADING':
            newState = Object.assign({}, state, {
                isHeroesComicsLoading: action.isLoading
            });
            return newState;
        default:
            return state;
    }
}

export default heroesReducer;