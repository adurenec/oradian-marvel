import { connect } from 'react-redux';
import createImageUrl from '../shared/createImageUrl.js';
import HeroDetailComponent from './HeroDetailComponent.jsx';

let mapStateToProps = state => {
    const { selectedHero, isHeroLoading } = state.heroes;

    if (!selectedHero) {
        return {
            imageURL: '',
            name: '',
            description: '',
            isHeroLoading: isHeroLoading
        }
    }

    const { name, description, thumbnail } = selectedHero;

    return {
        imageURL: createImageUrl(thumbnail),
        name,
        description,
        isHeroLoading
    };
};


export default connect(mapStateToProps)(HeroDetailComponent);