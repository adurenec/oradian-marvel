import api from '../shared/api.js';
import { KEY } from '../shared/api-key.js';
import { goToHeroDetail } from '../navigation/actions';
import { setIsTouched } from '../search/actions';

export function fetchHeroes(heroName = '') {

    return dispatch => {
        dispatch(setIsHeroListLoading(true));

        api.get(`/characters?nameStartsWith=${heroName}&limit=20&apikey=${KEY}`)
            .then(response => {

                console.log('response::', response);

                if (response.data && response.data.data && response.data.data.results) {
                    dispatch(setHeroList(response.data.data.results));
                }

            })
            .catch((error) => {
                console.log(error);
                alert('Ooops! We couldn\'t get data from MARVEL, please try again later...');
            })
            .then(() => {
                dispatch(setIsHeroListLoading(false));
                dispatch(setIsTouched());
            })
    };

}

export function fetchHeroByID(heroID = -1) {
    if (heroID < 0) {
        return;
    }

    return dispatch => {
        dispatch(setSelectedHero(null));
        dispatch(setSelectedHeroesComics([]));
        dispatch(setIsHeroDetailLoading(true));
        dispatch(goToHeroDetail());

        api.get(`/characters/${heroID}?apikey=${KEY}`)
            .then(response => {
                const responseData = response.data;
                console.log('response::', response);

                if (responseData && responseData.data && responseData.data.results && responseData.data.results[0]) {
                    dispatch(setSelectedHero(response.data.data.results[0]));
                    dispatch(fetchHeroesComics(heroID));
                }

            })
            .catch((error) => {
                console.log(error);
                alert('Ooops! We couldn\'t get data from MARVEL, please try again later...');
            })
            .then(() => {
                dispatch(setIsHeroDetailLoading(false));
            })
    };

}

export function fetchHeroesComics(heroID = -1) {
    if (heroID < 0) {
        return;
    }

    return dispatch => {
        dispatch(setSelectedHeroesComics([]));
        dispatch(setIsHeroesComicsLoading(true));

        api.get(`/characters/${heroID}/comics?format=comic&orderBy=-onsaleDate&limit=20&apikey=${KEY}`)
            .then(response => {

                console.log('response::', response);

                if (response.data && response.data.data && response.data.data.results) {
                    dispatch(setSelectedHeroesComics(response.data.data.results));
                }

            })
            .catch((error) => {
                console.log(error);
                alert('Ooops! We couldn\'t get data from MARVEL, please try again later...');
            })
            .then(() => {
                dispatch(setIsHeroesComicsLoading(false));
            })
    };

}

export function setHeroList(heroes = []) {
    return {
        type: 'SET_HERO_LIST',
        heroList: heroes
    };
}


export function setIsHeroListLoading(isLoading) {
    return {
        type: 'SET_IS_HERO_LIST_LOADING',
        isLoading: isLoading
    };
}

export function setSelectedHero(hero) {
    return {
        type: 'SET_SELECTED_HERO',
        selectedHero: hero
    };
}

export function setSelectedHeroesComics(comics) {
    return {
        type: 'SET_SELECTED_HEROES_COMICS',
        comics: comics
    };
}

export function setIsHeroDetailLoading(isLoading) {
    return {
        type: 'SET_IS_HERO_DETAIL_LOADING',
        isLoading: isLoading
    };
}

export function setIsHeroesComicsLoading(isLoading) {
    return {
        type: 'SET_IS_HERO_DETAIL_COMICS_LOADING',
        isLoading: isLoading
    };
}
