import React, {Component} from 'react';
import HeroDetailContainer from './HeroDetailContainer.jsx';
import HeroApperanceInComicsContainer from './HeroApperanceInComicsContainer.jsx';

class HeroDetailPage extends Component {

    render() {
        return (
            <article>
                <HeroDetailContainer/>

                <div className="row" style={{ marginTop: '30px' }}>
                    <h2 className="col-xs-12 detail__heading">Appears in comics:</h2>
                </div>

                <HeroApperanceInComicsContainer/>
            </article>
        );
    }
}

export default HeroDetailPage;
