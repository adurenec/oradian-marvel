import React, {Component} from 'react';
import Loader from '../shared/Loader.jsx';


class HeroDetailComponent extends Component {

    render() {
        const {name, imageURL, description, isHeroLoading} = this.props;

        if (isHeroLoading) {
            return <Loader/>;
        }

        return (
            <section className="row detail">
                <h1 className="col-xs-12 detail__heading">{name}</h1>
                <div className="col-xs-12 col-sm-4">
                    <figure className="detail__figure">
                        <img className="detail__image" alt={name} src={imageURL} />
                    </figure>
                </div>
                <div className="col-xs-12 col-sm-8">
                    <p className="detail__desc">
                        {description}
                    </p>
                </div>
            </section>
        );
    }
}

export default HeroDetailComponent;
