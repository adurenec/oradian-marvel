import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchComicByID } from '../comics/actions.js';
import Grid from '../grid/Grid.jsx';

const actionCreators = {
    fetchComicByID
};

let mapStateToProps = state => {
    const { selectedHeroComics, isHeroesComicsLoading } = state.heroes;
    const { isOnHomePage } = state.navigation;

    return {
        itemList: selectedHeroComics || [],
        isLoading: isHeroesComicsLoading,
        clickActionName: 'fetchComicByID',
        isOnHomePage: isOnHomePage,
        isTouched: true
    };
};

let mapDispatchToProps = dispatch => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Grid);