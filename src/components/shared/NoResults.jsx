import React from 'react';


export default function NoResults(props) {
    return (
        <div className="col-xs-12 empty">
            <p className="empty__message">{props.title}</p>
        </div>
    );
}