import React from 'react';


export default function Loader(props) {
    return (
        <div className="loader">
            <div className="loader__spinner"></div>
        </div>
    );
}