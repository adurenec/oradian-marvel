
export default function (thumbnail = {}) {
    if (thumbnail && thumbnail.path && thumbnail.extension) {
        return `${thumbnail.path}.${thumbnail.extension}`;
    }

    return '';
}