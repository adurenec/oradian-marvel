import React, {Component} from 'react';
import HomePage from './HomePage.jsx';
import ComicDetailPage from '../comics/ComicDetailPage.jsx';
import HeroDetailPage from '../heroes/HeroDetailPage.jsx';
import BackToHomeButton from './BackToHomeButton.jsx';


class NavigationComponent extends Component {
    render() {
        const { isOnHomePage, isOnHeroDetailPage, isOnComicDetailPage, goToHome } = this.props;

        if (isOnHomePage) return <HomePage/>;

        if (isOnComicDetailPage) return [<BackToHomeButton onClickAction={goToHome} />, <ComicDetailPage/>];

        if (isOnHeroDetailPage) return [<BackToHomeButton onClickAction={goToHome} />, <HeroDetailPage/>];
    }
}

export default NavigationComponent;
