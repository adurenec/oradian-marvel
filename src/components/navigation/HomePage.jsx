import React, {Component} from 'react';

import SearchContainer from '../search/SearchContainer.jsx';
import GridContainer from '../grid/GridContainer.jsx';

class HomePage extends Component {
    render() {
        return (
            <div className="container">
                <SearchContainer/>
                <GridContainer/>
            </div>
        );
    }
}

export default HomePage;
