

export function goToHome() {
    return {
        type: 'SET_NAVIGATION_TO_HOME'
    };
}


export function goToHeroDetail() {
    return {
        type: 'SET_NAVIGATION_TO_HERO_DETAIL'
    };
}


export function goToComicDetail() {
    return {
        type: 'SET_NAVIGATION_TO_COMIC_DETAIL'
    };
}
