import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goToHome } from './actions.js';
import NavigationComponent from './NavigationComponent.jsx';

const actionCreators = {
    goToHome
};


let mapStateToProps = state => {
    const { isOnHomePage, isOnHeroDetailPage, isOnComicDetailPage } = state.navigation;

    return {
        isOnHomePage,
        isOnHeroDetailPage,
        isOnComicDetailPage
    };
};

let mapDispatchToProps = dispatch => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(NavigationComponent);