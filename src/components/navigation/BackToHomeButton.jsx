import React from 'react';


export default function BackToHomeButton(props) {
    return (
        <a onClick={() => props.onClickAction()} className="navigation__button navigation__button--floating">
            Back to search
        </a>
    );
}