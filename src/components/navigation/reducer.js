
function navigationReducer(state = {}, action) {
    let newState;
    switch (action.type) {
        case 'SET_NAVIGATION_TO_HOME':
            newState = Object.assign({}, state, {
                isOnHomePage: true,
                isOnHeroDetailPage: false,
                isOnComicDetailPage: false
            });
            return newState;
        case 'SET_NAVIGATION_TO_HERO_DETAIL':
            newState = Object.assign({}, state, {
                isOnHomePage: false,
                isOnHeroDetailPage: true,
                isOnComicDetailPage: false
            });
            return newState;
        case 'SET_NAVIGATION_TO_COMIC_DETAIL':
            newState = Object.assign({}, state, {
                isOnHomePage: false,
                isOnHeroDetailPage: false,
                isOnComicDetailPage: true
            });
            return newState;
        default:
            return state;
    }
}

export default navigationReducer;