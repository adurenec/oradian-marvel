import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchHeroByID } from '../heroes/actions.js';
import Grid from '../grid/Grid.jsx';

const actionCreators = {
    fetchHeroByID
};

let mapStateToProps = state => {
    const { selectedComicHeroes, isComicsHeroesLoading } = state.comics;
    const { isOnHomePage } = state.navigation;

    return {
        itemList: selectedComicHeroes || [],
        isLoading: isComicsHeroesLoading,
        clickActionName: 'fetchHeroByID',
        isOnHomePage: isOnHomePage,
        isTouched: true
    };
};

let mapDispatchToProps = dispatch => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Grid);