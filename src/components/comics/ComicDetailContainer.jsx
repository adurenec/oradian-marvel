import { connect } from 'react-redux';
import createImageUrl from '../shared/createImageUrl.js';
import ComicDetailComponent from './ComicDetailComponent.jsx';

let mapStateToProps = state => {
    const { selectedComic, isComicLoading } = state.comics;

    if (!selectedComic) {
        return {
            imageURL: '',
            title: '',
            description: '',
            isComicLoading: isComicLoading,
        };
    }

    const { title, description, thumbnail } = selectedComic;

    return {
        imageURL: createImageUrl(thumbnail),
        title: title,
        description: description,
        isComicLoading: isComicLoading,
    };
};

export default connect(mapStateToProps)(ComicDetailComponent);