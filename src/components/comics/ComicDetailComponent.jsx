import React, {Component} from 'react';
import Loader from '../shared/Loader.jsx';

class ComicDetailComponent extends Component {

    render() {
        const { title, imageURL, description, isComicLoading } = this.props;

        if (isComicLoading) {
            return <Loader/>;
        }

        return (
            <section className="row detail">
                <h1 className="col-xs-12 detail__heading">{title}</h1>
                <div className="col-xs-12 col-sm-4">
                    <figure className="detail__figure">
                        <img className="detail__image" alt={title} src={imageURL} />
                    </figure>
                </div>
                <div className="col-xs-12 col-sm-8">
                    <p className="detail__desc">
                        {description}
                    </p>
                </div>
            </section>
        );
    }
}

export default ComicDetailComponent;
