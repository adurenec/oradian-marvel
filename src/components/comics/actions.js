import api from '../shared/api.js';
import { KEY } from '../shared/api-key.js';
import { goToComicDetail } from '../navigation/actions';
import { setIsTouched } from '../search/actions';

export function fetchComics(comicName = '') {

    return dispatch => {
        dispatch(setIsComicListLoading(true));

        api.get(`/comics?titleStartsWith=${comicName}&orderBy=-onsaleDate&limit=20&apikey=${KEY}`)
            .then(response => {

                console.log('response::', response);

                if (response.data && response.data.data && response.data.data.results) {
                    dispatch(setComicList(response.data.data.results));
                }

            })
            .catch((error) => {
                console.log(error);
                alert('Ooops! We couldn\'t get data from MARVEL, please try again later...');
            })
            .then(() => {
                dispatch(setIsComicListLoading(false));
                dispatch(setIsTouched());
            })
    };

}

export function fetchComicByID(comicID = -1) {
    if (comicID < 0) {
        return;
    }

    return dispatch => {
        dispatch(setSelectedComic(null));
        dispatch(setSelectedComicsHeroes([]));
        dispatch(setIsComicDetailLoading(true));
        dispatch(goToComicDetail());

        api.get(`/comics/${comicID}?apikey=${KEY}`)
            .then(response => {
                const responseData = response.data;
                console.log('response::', response);

                if (responseData && responseData.data && responseData.data.results && responseData.data.results[0]) {
                    dispatch(setSelectedComic(response.data.data.results[0]));
                    dispatch(fetchComicsHeroes(comicID));
                }

            })
            .catch((error) => {
                console.log(error);
                alert('Ooops! We couldn\'t get data from MARVEL, please try again later...');
            })
            .then(() => {
                dispatch(setIsComicDetailLoading(false));
            })
    };

}

export function fetchComicsHeroes(comicID = -1) {
    if (comicID < 0) {
        return;
    }

    return dispatch => {
        dispatch(setSelectedComicsHeroes([]));
        dispatch(setIsComicHeroesLoading(true));

        api.get(`/comics/${comicID}/characters?limit=20&apikey=${KEY}`)
            .then(response => {

                console.log('response::', response);

                if (response.data && response.data.data && response.data.data.results) {
                    dispatch(setSelectedComicsHeroes(response.data.data.results));
                }

            })
            .catch((error) => {
                console.log(error);
                alert('Ooops! We couldn\'t get data from MARVEL, please try again later...');
            })
            .then(() => {
                dispatch(setIsComicHeroesLoading(false));
            })
    };

}


export function setComicList(comics) {
    return {
        type: 'SET_COMIC_LIST',
        comics: comics
    };
}


export function setIsComicListLoading(isLoading) {
    return {
        type: 'SET_IS_COMIC_LIST_LOADING',
        isLoading: isLoading
    };
}


export function setSelectedComic(comic) {
    return {
        type: 'SET_SELECTED_COMIC',
        selectedComic: comic
    };
}

export function setSelectedComicsHeroes(heroes) {
    return {
        type: 'SET_SELECTED_COMICS_HEROES',
        heroes: heroes
    };
}

export function setIsComicDetailLoading(isLoading) {
    return {
        type: 'SET_IS_COMIC_DETAIL_LOADING',
        isLoading: isLoading
    };
}

export function setIsComicHeroesLoading(isLoading) {
    return {
        type: 'SET_IS_COMIC_DETAIL_HEROES_LOADING',
        isLoading: isLoading
    };
}
