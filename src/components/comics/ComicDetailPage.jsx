import React, {Component} from 'react';
import ComicDetailContainer from './ComicDetailContainer.jsx';
import ComicCharacterApperanceContainer from './ComicCharacterApperanceContainer.jsx';

class ComicDetailPage extends Component {

    render() {
        return (
            <article className="detail__page-wrap">
                <ComicDetailContainer/>

                <div className="row" style={{ marginTop: '30px' }}>
                    <h2 className="col-xs-12 detail__heading">Characters appearing:</h2>
                </div>

                <ComicCharacterApperanceContainer />
            </article>
        );
    }
}

export default ComicDetailPage;
