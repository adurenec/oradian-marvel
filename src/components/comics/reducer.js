
function comicsReducer(state = {}, action) {
    let newState;
    switch (action.type) {
        case 'SET_COMIC_LIST':
            newState = Object.assign({}, state, {
                comicList: action.comics
            });
            return newState;
        case 'SET_IS_COMIC_LIST_LOADING':
            newState = Object.assign({}, state, {
                isLoading: action.isLoading
            });
            return newState;
        case 'SET_SELECTED_COMIC':
            newState = Object.assign({}, state, {
                selectedComic: action.selectedComic
            });
            return newState;
        case 'SET_IS_COMIC_DETAIL_LOADING':
            newState = Object.assign({}, state, {
                isComicLoading: action.isLoading
            });
            return newState;
        case 'SET_SELECTED_COMICS_HEROES':
            newState = Object.assign({}, state, {
                selectedComicHeroes: action.heroes
            });
            return newState;
        case 'SET_IS_COMIC_DETAIL_HEROES_LOADING':
            newState = Object.assign({}, state, {
                isComicsHeroesLoading: action.isLoading
            });
            return newState;
        default:
            return state;
    }
}

export default comicsReducer;