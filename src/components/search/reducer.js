
function searchReducer(state = {}, action) {
    let newState;
    switch (action.type) {
        case 'SET_SEARCH_TERM':
            newState = Object.assign({}, state, {
                searchTerm: action.searchTerm
            });
            return newState;
        case 'SET_TO_HERO_SEARCH':
            newState = Object.assign({}, state, {
                isHeroSearch: true,
                isComicSearch: false
            });
            return newState;
        case 'SET_TO_COMIC_SEARCH':
            newState = Object.assign({}, state, {
                isHeroSearch: false,
                isComicSearch: true
            });
            return newState;
        case 'SET_IS_TOUCHED':
            newState = Object.assign({}, state, {
                isTouched: true
            });
            return newState;
        default:
            return state;
    }
}

export default searchReducer;