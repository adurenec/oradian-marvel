import React, {Component} from 'react';
import ComicHeroPickerContainer from './ComicHeroPickerContainer.jsx';
import Input from './Input.jsx';
import SubmitButton from './SubmitButton.jsx';

class SearchComponent extends Component {
    render() {
        const { searchTerm, setSearchTerm, fetchFunctionName } = this.props;

        return (
            <div className="search__wrapper">
                <ComicHeroPickerContainer/>
                <Input value={searchTerm} onChangeAction={setSearchTerm} />
                <SubmitButton value={searchTerm} onClickAction={() => this.props[fetchFunctionName](searchTerm)} />
            </div>
        );
    }
}

export default SearchComponent;
