

export function setSearchTerm(term) {
    return {
        type: 'SET_SEARCH_TERM',
        searchTerm: term
    };
}


export function switchToHeroSearch() {
    return {
        type: 'SET_TO_HERO_SEARCH'
    };
}


export function switchToComicSearch() {
    return {
        type: 'SET_TO_COMIC_SEARCH'
    };
}


export function setIsTouched() {
    return {
        type: 'SET_IS_TOUCHED'
    };
}
