import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setSearchTerm, switchToHeroSearch, switchToComicSearch } from './actions.js';
import { fetchHeroes } from '../heroes/actions.js';
import { fetchComics } from '../comics/actions';
import SearchComponent from './SearchComponent.jsx';

const actionCreators = {
    setSearchTerm,
    switchToHeroSearch,
    switchToComicSearch,
    fetchHeroes,
    fetchComics
};

let mapStateToProps = state => {
    console.log('state',state);

    const { isHeroSearch, isComicSearch, searchTerm } = state.search;
    let searchAction = isHeroSearch ? 'fetchHeroes' : 'fetchComics';

    return {
        searchTerm: searchTerm,
        isHeroSearch: isHeroSearch,
        isComicSearch: isComicSearch,
        fetchFunctionName: searchAction
    };
};

let mapDispatchToProps = dispatch => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchComponent);