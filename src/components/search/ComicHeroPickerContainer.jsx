import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { switchToHeroSearch, switchToComicSearch } from './actions.js';
import ComicHeroPicker from './ComicHeroPicker.jsx';

const actionCreators = {
    switchToHeroSearch,
    switchToComicSearch
};

let mapStateToProps = state => {
    return {
        isHeroSearch: state.search.isHeroSearch,
        isComicSearch: state.search.isComicSearch,
    };
};

let mapDispatchToProps = dispatch => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(ComicHeroPicker);