import React, {Component} from 'react';

class Input extends Component {
    render() {
        return (
            <div>
                <input type="text" className="search__bar" value={this.props.value} onChange={evt => this.props.onChangeAction(evt.target.value)} />
            </div>
        );
    }
}

export default Input;