import React, {Component} from 'react';

class ComicHeroPicker extends Component {
    render() {
        const {switchToHeroSearch, switchToComicSearch, isHeroSearch, isComicSearch} = this.props;

        return (
            <section className="search-picker">
                <button className={'search-picker__button ' + (isHeroSearch ? 'search-picker__button--active' : '')}
                        onClick={() => switchToHeroSearch()}>
                    HEROES
                </button>
                <button className={'search-picker__button ' + (isComicSearch ? 'search-picker__button--active' : '')}
                        onClick={() => switchToComicSearch()}>
                    COMICS
                </button>
            </section>
        )
    }
}

export default ComicHeroPicker;