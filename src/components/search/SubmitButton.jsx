import React from 'react';


export default function SubmitButton(props) {
    return (
        <button onClick={() => props.onClickAction()} className="search__button" disabled={props.value.length < 1}>
            GO
        </button>
    );
}