import React, {Component} from 'react';
import GridTile from './GridTile.jsx';
import NoResults from '../shared/NoResults.jsx';
import Loader from '../shared/Loader.jsx';
import createImageUrl from '../shared/createImageUrl.js';

class Grid extends Component {
    buildTiles(items) {
        return items.map(item => {
            let { name, title } = item;


            return (
                <GridTile key={item.id}
                          onClickAction={() => this.props[this.props.clickActionName](item.id)}
                          title={name || title}
                          imageURL={createImageUrl(item.thumbnail)} />
            )
        })
    }

    showResult() {
        const { itemList, isLoading, isTouched, isOnHomePage } = this.props;
        const noResultsComponent = <NoResults title='Nothing to display...' />;

        if (isOnHomePage) {
            if (itemList.length <= 0 && !isLoading && isTouched) {
                return noResultsComponent;
            }
        } else {
            if (itemList.length <= 0 && !isLoading) {
                return noResultsComponent;
            }
        }

        // if (itemList.length <= 0 && !isLoading && isTouched) {
        //     return (<NoResults title='Nothing to display...' />);
        // }

        return this.buildTiles(itemList);
    }

    render() {
        return (
            <div className="row">
                <div style={{ margin: '30px 0 20px 0' }}>
                    {
                        this.props.isLoading ? (
                            <Loader/>
                        ) : null
                    }

                    { this.showResult() }
                </div>
            </div>
        );
    }
}

export default Grid;
