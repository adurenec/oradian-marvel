import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchHeroByID } from '../heroes/actions.js';
import { fetchComicByID } from '../comics/actions';
import Grid from './Grid.jsx';

const actionCreators = {
    fetchComicByID,
    fetchHeroByID
};


let mapStateToProps = state => {
    const { isHeroSearch, isTouched } = state.search;
    const { isOnHomePage } = state.navigation;
    const isHeroListLoading = state.heroes.isLoading;
    const isComicListLoading = state.comics.isLoading;
    const items = isHeroSearch ? state.heroes.heroList : state.comics.comicList;
    const actionName = isHeroSearch ? 'fetchHeroByID' : 'fetchComicByID';
    const isLoadingFlag = isHeroSearch ? isHeroListLoading : isComicListLoading;

    return {
        isLoading: isLoadingFlag,
        isTouched: isTouched,
        isOnHomePage: isOnHomePage,
        itemList: items || [],
        clickActionName: actionName
    };
};

let mapDispatchToProps = dispatch => {
    return bindActionCreators(actionCreators, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Grid);