import React, {Component} from 'react';

class GridTile extends Component {
    render() {
        return (
            <section className="col-xs-12 col-sm-6 col-md-3">
                <div className="grid-tile" onClick={() => this.props.onClickAction()}>
                    <figure className="grid-tile__figure">
                        <img className="grid-tile__image" src={this.props.imageURL} alt={this.props.title} />
                    </figure>
                    <h4 className="grid-tile__heading">{ this.props.title }</h4>
                </div>
            </section>
        )
    }
}

export default GridTile;
