import React, {Component} from 'react';
import {Provider} from 'react-redux';
import store from './redux/store.js';

import NavigationContainer from './components/navigation/NavigationContainer.jsx';
import './App.scss';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="container">
                    <NavigationContainer/>
                </div>
            </Provider>
        );
    }
}

export default App;
